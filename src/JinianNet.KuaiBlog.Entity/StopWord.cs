﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// StopWords:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class StopWord
	{
		public StopWord()
		{}
		#region Model
		private int _stopwordrowid;
		private Guid _blogid;
		private string _stopword;
		/// <summary>
		/// 
		/// </summary>
		public int StopWordRowId
		{
			set{ _stopwordrowid=value;}
			get{return _stopwordrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Word
		{
			set{ _stopword=value;}
			get{return _stopword;}
		}
		#endregion Model

	}
}

