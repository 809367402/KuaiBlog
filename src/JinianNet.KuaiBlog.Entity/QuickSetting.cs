﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// QuickSettings:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class QuickSetting
	{
		public QuickSetting()
		{}
		#region Model
		private int _quicksettingid;
		private Guid _blogid;
		private string _username;
		private string _settingname;
		private string _settingvalue;
		/// <summary>
		/// 
		/// </summary>
		public int QuickSettingId
		{
			set{ _quicksettingid=value;}
			get{return _quicksettingid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SettingName
		{
			set{ _settingname=value;}
			get{return _settingname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SettingValue
		{
			set{ _settingvalue=value;}
			get{return _settingvalue;}
		}
		#endregion Model

	}
}

