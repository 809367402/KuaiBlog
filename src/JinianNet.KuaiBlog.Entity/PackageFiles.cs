﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// PackageFiles:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class PackageFiles
	{
		public PackageFiles()
		{}
		#region Model
		private string _packageid;
		private int _fileorder;
		private string _filepath;
		private bool _isdirectory;
		/// <summary>
		/// 
		/// </summary>
		public string PackageId
		{
			set{ _packageid=value;}
			get{return _packageid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int FileOrder
		{
			set{ _fileorder=value;}
			get{return _fileorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FilePath
		{
			set{ _filepath=value;}
			get{return _filepath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsDirectory
		{
			set{ _isdirectory=value;}
			get{return _isdirectory;}
		}
		#endregion Model

	}
}

