﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// RightRoles:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class RightRole
	{
		public RightRole()
		{}
		#region Model
		private int _rightrolerowid;
		private Guid _blogid;
		private string _rightname;
		private string _role;
		/// <summary>
		/// 
		/// </summary>
		public int RightRoleRowId
		{
			set{ _rightrolerowid=value;}
			get{return _rightrolerowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RightName
		{
			set{ _rightname=value;}
			get{return _rightname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Role
		{
			set{ _role=value;}
			get{return _role;}
		}
		#endregion Model

	}
}

