﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// PostCategory:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class PostCategory
	{
		public PostCategory()
		{}
		#region Model
		private int _postcategoryid;
		private Guid _blogid;
		private Guid _postid;
		private Guid _categoryid;
		/// <summary>
		/// 
		/// </summary>
		public int PostCategoryId
		{
			set{ _postcategoryid=value;}
			get{return _postcategoryid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid PostId
		{
			set{ _postid=value;}
			get{return _postid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid CategoryId
		{
			set{ _categoryid=value;}
			get{return _categoryid;}
		}
		#endregion Model

	}
}

