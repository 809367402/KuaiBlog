﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Roles:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Role
	{
		public Role()
		{}
		#region Model
		private int _roleid;
		private Guid _blogid;
		private string _role;
		/// <summary>
		/// 
		/// </summary>
		public int RoleId
		{
			set{ _roleid=value;}
			get{return _roleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RoleName
		{
			set{ _role=value;}
			get{return _role;}
		}
		#endregion Model

	}
}

