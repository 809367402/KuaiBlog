﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// PingService:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class PingService
	{
		public PingService()
		{}
		#region Model
		private int _pingserviceid;
		private Guid _blogid;
		private string _link;
		/// <summary>
		/// 
		/// </summary>
		public int PingServiceId
		{
			set{ _pingserviceid=value;}
			get{return _pingserviceid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Link
		{
			set{ _link=value;}
			get{return _link;}
		}
		#endregion Model

	}
}

