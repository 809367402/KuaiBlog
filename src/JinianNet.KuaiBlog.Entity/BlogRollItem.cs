﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// BlogRollItems:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class BlogRollItem
	{
		public BlogRollItem()
		{}
		#region Model
		private int _blogrollrowid;
		private Guid _blogid;
		private Guid _blogrollid;
		private string _title;
		private string _description;
		private string _blogurl;
		private string _feedurl;
		private string _xfn;
		private int _sortindex;
		/// <summary>
		/// 
		/// </summary>
		public int BlogRollRowId
		{
			set{ _blogrollrowid=value;}
			get{return _blogrollrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogRollId
		{
			set{ _blogrollid=value;}
			get{return _blogrollid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BlogUrl
		{
			set{ _blogurl=value;}
			get{return _blogurl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FeedUrl
		{
			set{ _feedurl=value;}
			get{return _feedurl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Xfn
		{
			set{ _xfn=value;}
			get{return _xfn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SortIndex
		{
			set{ _sortindex=value;}
			get{return _sortindex;}
		}
		#endregion Model

	}
}

