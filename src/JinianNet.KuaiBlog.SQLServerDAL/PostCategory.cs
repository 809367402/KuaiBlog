﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:PostCategory
	/// </summary>
    public partial class PostCategory : BaseDAL
	{
		public PostCategory()
		{}



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(JinianNet.KuaiBlog.Entity.PostCategory model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into be_PostCategory(");
			strSql.Append("BlogId,PostId,CategoryId)");
			strSql.Append(" values (");
			strSql.Append("@BlogId,@PostId,@CategoryId)");
			strSql.Append(";select @@IdENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CategoryId", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = Guid.NewGuid();
			parameters[1].Value = Guid.NewGuid();
			parameters[2].Value = Guid.NewGuid();

			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.PostCategory model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_PostCategory set ");
			strSql.Append("BlogId=@BlogId,");
			strSql.Append("PostId=@PostId,");
			strSql.Append("CategoryId=@CategoryId");
			strSql.Append(" where PostCategoryId=@PostCategoryId");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CategoryId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostCategoryId", SqlDbType.Int,4)};
			parameters[0].Value = model.BlogId;
			parameters[1].Value = model.PostId;
			parameters[2].Value = model.CategoryId;
			parameters[3].Value = model.PostCategoryId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int PostCategoryId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_PostCategory ");
			strSql.Append(" where PostCategoryId=@PostCategoryId");
			SqlParameter[] parameters = {
					new SqlParameter("@PostCategoryId", SqlDbType.Int,4)
			};
			parameters[0].Value = PostCategoryId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string PostCategoryIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_PostCategory ");
			strSql.Append(" where PostCategoryId in ("+PostCategoryIdlist + ")  ");
			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.PostCategory GetItem(int PostCategoryId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 PostCategoryId,BlogId,PostId,CategoryId from be_PostCategory ");
			strSql.Append(" where PostCategoryId=@PostCategoryId");
			SqlParameter[] parameters = {
					new SqlParameter("@PostCategoryId", SqlDbType.Int,4)
			};
			parameters[0].Value = PostCategoryId;

			JinianNet.KuaiBlog.Entity.PostCategory model=new JinianNet.KuaiBlog.Entity.PostCategory();
			DataTable dt=Helper.ExecuteTable(CommandType.Text,strSql.ToString(),parameters);
			if(dt.Rows.Count>0)
			{
				return DataRowToModel(dt.Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.PostCategory DataRowToModel(DataRow row)
		{
			JinianNet.KuaiBlog.Entity.PostCategory model=new JinianNet.KuaiBlog.Entity.PostCategory();
			if (row != null)
			{
				if(row["PostCategoryId"]!=null && row["PostCategoryId"].ToString()!="")
				{
					model.PostCategoryId=int.Parse(row["PostCategoryId"].ToString());
				}
				if(row["BlogId"]!=null && row["BlogId"].ToString()!="")
				{
					model.BlogId= new Guid(row["BlogId"].ToString());
				}
				if(row["PostId"]!=null && row["PostId"].ToString()!="")
				{
					model.PostId= new Guid(row["PostId"].ToString());
				}
				if(row["CategoryId"]!=null && row["CategoryId"].ToString()!="")
				{
					model.CategoryId= new Guid(row["CategoryId"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataTable GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select PostCategoryId,BlogId,PostId,CategoryId ");
			strSql.Append(" FROM be_PostCategory ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataTable GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" PostCategoryId,BlogId,PostId,CategoryId ");
			strSql.Append(" FROM be_PostCategory ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM be_PostCategory ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.PostCategoryId desc");
			}
			strSql.Append(")AS Row, T.*  from be_PostCategory T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "be_PostCategory";
			parameters[1].Value = "PostCategoryId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		

		
	}
}

