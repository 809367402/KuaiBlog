﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
    public class Transaction
    {
        private CommandType _commandtype;
        private string _text;
        private DbParameter[] _parameter;

        public CommandType CommandType
        {
            get { return _commandtype; }
            set { _commandtype = value; }
        }
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
        public DbParameter[] Parameter
        {
            get { return _parameter; }
            set { _parameter = value; }
        }
    }

    public class TransactionCollection : List<Transaction>
    {
        public TransactionCollection()
            : base()
        {
        }

        public TransactionCollection(int capacity)
            : base(capacity)
        {
        }

        public TransactionCollection(IEnumerable<Transaction> collection)
            : base(collection)
        {

        }

        public TransactionCollection(string[] collection)
            : this(collection.Length)
        {
            this.AddRange(collection);
        }

        public void Add(string item)
        {
            Transaction t = new Transaction();
            t.CommandType = CommandType.Text;
            t.Text = item;
            t.Parameter = null;
            this.Add(t);
        }

        public void Add(CommandType commandType, string cmdText)
        {
            Transaction t = new Transaction();
            t.CommandType = commandType;
            t.Text = cmdText;
            t.Parameter = null;
            this.Add(t);
        }

        public void AddRange(string[] collection)
        {
            for (int i = 0; i < collection.Length; i++)
            {
                if (!string.IsNullOrEmpty(collection[i]))
                {
                    this.Add(collection[i]);
                }
            }
        }
    }

}