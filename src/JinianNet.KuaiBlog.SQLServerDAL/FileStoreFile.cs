﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:FileStoreFiles
	/// </summary>
    public partial class FileStoreFile : BaseDAL
	{
		public FileStoreFile()
		{}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.FileStoreFiles model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_FileStoreFiles set ");
			strSql.Append("ParentDirectoryId=@ParentDirectoryId,");
			strSql.Append("Name=@Name,");
			strSql.Append("FullPath=@FullPath,");
			strSql.Append("Contents=@Contents,");
			strSql.Append("Size=@Size,");
			strSql.Append("CreateDate=@CreateDate,");
			strSql.Append("LastAccess=@LastAccess,");
			strSql.Append("LastModify=@LastModify");
			strSql.Append(" where FileId=@FileId ");
			SqlParameter[] parameters = {
					new SqlParameter("@ParentDirectoryId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Name", SqlDbType.VarChar,255),
					new SqlParameter("@FullPath", SqlDbType.VarChar,255),
					new SqlParameter("@Contents", SqlDbType.VarBinary,-1),
					new SqlParameter("@Size", SqlDbType.Int,4),
					new SqlParameter("@CreateDate", SqlDbType.DateTime),
					new SqlParameter("@LastAccess", SqlDbType.DateTime),
					new SqlParameter("@LastModify", SqlDbType.DateTime),
					new SqlParameter("@FileId", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.ParentDirectoryId;
			parameters[1].Value = model.Name;
			parameters[2].Value = model.FullPath;
			parameters[3].Value = model.Contents;
			parameters[4].Value = model.Size;
			parameters[5].Value = model.CreateDate;
			parameters[6].Value = model.LastAccess;
			parameters[7].Value = model.LastModify;
			parameters[8].Value = model.FileId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid FileId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_FileStoreFiles ");
			strSql.Append(" where FileId=@FileId ");
			SqlParameter[] parameters = {
					new SqlParameter("@FileId", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = FileId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string FileIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_FileStoreFiles ");
			strSql.Append(" where FileId in ("+FileIdlist + ")  ");
			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.FileStoreFiles GetItem(Guid FileId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 FileId,ParentDirectoryId,Name,FullPath,Contents,Size,CreateDate,LastAccess,LastModify from be_FileStoreFiles ");
			strSql.Append(" where FileId=@FileId ");
			SqlParameter[] parameters = {
					new SqlParameter("@FileId", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = FileId;

			JinianNet.KuaiBlog.Entity.FileStoreFiles model=new JinianNet.KuaiBlog.Entity.FileStoreFiles();
			DataTable dt=Helper.ExecuteTable(CommandType.Text,strSql.ToString(),parameters);
			if(dt.Rows.Count>0)
			{
				return DataRowToModel(dt.Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.FileStoreFiles DataRowToModel(DataRow row)
		{
			JinianNet.KuaiBlog.Entity.FileStoreFiles model=new JinianNet.KuaiBlog.Entity.FileStoreFiles();
			if (row != null)
			{
				if(row["FileId"]!=null && row["FileId"].ToString()!="")
				{
					model.FileId= new Guid(row["FileId"].ToString());
				}
				if(row["ParentDirectoryId"]!=null && row["ParentDirectoryId"].ToString()!="")
				{
					model.ParentDirectoryId= new Guid(row["ParentDirectoryId"].ToString());
				}
				if(row["Name"]!=null)
				{
					model.Name=row["Name"].ToString();
				}
				if(row["FullPath"]!=null)
				{
					model.FullPath=row["FullPath"].ToString();
				}
				if(row["Contents"]!=null && row["Contents"].ToString()!="")
				{
					model.Contents=(byte[])row["Contents"];
				}
				if(row["Size"]!=null && row["Size"].ToString()!="")
				{
					model.Size=int.Parse(row["Size"].ToString());
				}
				if(row["CreateDate"]!=null && row["CreateDate"].ToString()!="")
				{
					model.CreateDate=DateTime.Parse(row["CreateDate"].ToString());
				}
				if(row["LastAccess"]!=null && row["LastAccess"].ToString()!="")
				{
					model.LastAccess=DateTime.Parse(row["LastAccess"].ToString());
				}
				if(row["LastModify"]!=null && row["LastModify"].ToString()!="")
				{
					model.LastModify=DateTime.Parse(row["LastModify"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataTable GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select FileId,ParentDirectoryId,Name,FullPath,Contents,Size,CreateDate,LastAccess,LastModify ");
			strSql.Append(" FROM be_FileStoreFiles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataTable GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" FileId,ParentDirectoryId,Name,FullPath,Contents,Size,CreateDate,LastAccess,LastModify ");
			strSql.Append(" FROM be_FileStoreFiles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM be_FileStoreFiles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.FileId desc");
			}
			strSql.Append(")AS Row, T.*  from be_FileStoreFiles T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "be_FileStoreFiles";
			parameters[1].Value = "FileId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		

		
	}
}

