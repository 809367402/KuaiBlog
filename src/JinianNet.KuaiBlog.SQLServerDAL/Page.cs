﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Pages
    /// </summary>
    public partial class Page : BaseDAL
    {
        public Page()
        { }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(JinianNet.KuaiBlog.Entity.Page model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into be_Pages(");
            strSql.Append("BlogId,PageId,Title,Description,PageContent,Keywords,DateCreated,DateModified,IsPublished,IsFrontPage,Parent,ShowInList,Slug,IsDeleted)");
            strSql.Append(" values (");
            strSql.Append("@BlogId,@PageId,@Title,@Description,@PageContent,@Keywords,@DateCreated,@DateModified,@IsPublished,@IsFrontPage,@Parent,@ShowInList,@Slug,@IsDeleted)");
            strSql.Append(";select @@IdENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PageId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Title", SqlDbType.NVarChar,255),
					new SqlParameter("@Description", SqlDbType.NVarChar,-1),
					new SqlParameter("@PageContent", SqlDbType.NVarChar,-1),
					new SqlParameter("@Keywords", SqlDbType.NVarChar,-1),
					new SqlParameter("@DateCreated", SqlDbType.DateTime),
					new SqlParameter("@DateModified", SqlDbType.DateTime),
					new SqlParameter("@IsPublished", SqlDbType.Bit,1),
					new SqlParameter("@IsFrontPage", SqlDbType.Bit,1),
					new SqlParameter("@Parent", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@ShowInList", SqlDbType.Bit,1),
					new SqlParameter("@Slug", SqlDbType.NVarChar,255),
					new SqlParameter("@IsDeleted", SqlDbType.Bit,1)};
            parameters[0].Value = Guid.NewGuid();
            parameters[1].Value = Guid.NewGuid();
            parameters[2].Value = model.Title;
            parameters[3].Value = model.Description;
            parameters[4].Value = model.PageContent;
            parameters[5].Value = model.Keywords;
            parameters[6].Value = model.DateCreated;
            parameters[7].Value = model.DateModified;
            parameters[8].Value = model.IsPublished;
            parameters[9].Value = model.IsFrontPage;
            parameters[10].Value = Guid.NewGuid();
            parameters[11].Value = model.ShowInList;
            parameters[12].Value = model.Slug;
            parameters[13].Value = model.IsDeleted;

            object obj = Helper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(JinianNet.KuaiBlog.Entity.Page model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update be_Pages set ");
            strSql.Append("BlogId=@BlogId,");
            strSql.Append("PageId=@PageId,");
            strSql.Append("Title=@Title,");
            strSql.Append("Description=@Description,");
            strSql.Append("PageContent=@PageContent,");
            strSql.Append("Keywords=@Keywords,");
            strSql.Append("DateCreated=@DateCreated,");
            strSql.Append("DateModified=@DateModified,");
            strSql.Append("IsPublished=@IsPublished,");
            strSql.Append("IsFrontPage=@IsFrontPage,");
            strSql.Append("Parent=@Parent,");
            strSql.Append("ShowInList=@ShowInList,");
            strSql.Append("Slug=@Slug,");
            strSql.Append("IsDeleted=@IsDeleted");
            strSql.Append(" where PageRowId=@PageRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PageId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Title", SqlDbType.NVarChar,255),
					new SqlParameter("@Description", SqlDbType.NVarChar,-1),
					new SqlParameter("@PageContent", SqlDbType.NVarChar,-1),
					new SqlParameter("@Keywords", SqlDbType.NVarChar,-1),
					new SqlParameter("@DateCreated", SqlDbType.DateTime),
					new SqlParameter("@DateModified", SqlDbType.DateTime),
					new SqlParameter("@IsPublished", SqlDbType.Bit,1),
					new SqlParameter("@IsFrontPage", SqlDbType.Bit,1),
					new SqlParameter("@Parent", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@ShowInList", SqlDbType.Bit,1),
					new SqlParameter("@Slug", SqlDbType.NVarChar,255),
					new SqlParameter("@IsDeleted", SqlDbType.Bit,1),
					new SqlParameter("@PageRowId", SqlDbType.Int,4)};
            parameters[0].Value = model.BlogId;
            parameters[1].Value = model.PageId;
            parameters[2].Value = model.Title;
            parameters[3].Value = model.Description;
            parameters[4].Value = model.PageContent;
            parameters[5].Value = model.Keywords;
            parameters[6].Value = model.DateCreated;
            parameters[7].Value = model.DateModified;
            parameters[8].Value = model.IsPublished;
            parameters[9].Value = model.IsFrontPage;
            parameters[10].Value = model.Parent;
            parameters[11].Value = model.ShowInList;
            parameters[12].Value = model.Slug;
            parameters[13].Value = model.IsDeleted;
            parameters[14].Value = model.PageRowId;

            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int PageRowId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from be_Pages ");
            strSql.Append(" where PageRowId=@PageRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@PageRowId", SqlDbType.Int,4)
			};
            parameters[0].Value = PageRowId;

            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string PageRowIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from be_Pages ");
            strSql.Append(" where PageRowId in (" + PageRowIdlist + ")  ");
            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public JinianNet.KuaiBlog.Entity.Page GetItem(int PageRowId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 PageRowId,BlogId,PageId,Title,Description,PageContent,Keywords,DateCreated,DateModified,IsPublished,IsFrontPage,Parent,ShowInList,Slug,IsDeleted from be_Pages ");
            strSql.Append(" where PageRowId=@PageRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@PageRowId", SqlDbType.Int,4)
			};
            parameters[0].Value = PageRowId;

            using (System.Data.Common.DbDataReader dr = Helper.ExecuteReader(CommandType.Text, strSql.ToString(), parameters))
            {
                if (dr.Read())
                {
                    return GetItem(dr);
                }
            }
            return null;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public JinianNet.KuaiBlog.Entity.Page GetItem(string slug)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 PageRowId,BlogId,PageId,Title,Description,PageContent,Keywords,DateCreated,DateModified,IsPublished,IsFrontPage,Parent,ShowInList,Slug,IsDeleted from be_Pages ");
            strSql.Append(" where Slug=@Slug");
            SqlParameter[] parameters = {
					new SqlParameter("@Slug", SqlDbType.NVarChar,50)
			};
            using (System.Data.Common.DbDataReader dr = Helper.ExecuteReader(CommandType.Text, strSql.ToString(), parameters))
            {
                if (dr.Read())
                {
                    return GetItem(dr);
                }
            }
            return null;
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        private JinianNet.KuaiBlog.Entity.Page GetItem(IDataReader row)
        {
            JinianNet.KuaiBlog.Entity.Page model = new JinianNet.KuaiBlog.Entity.Page();

            if (row["PageRowId"] != null && row["PageRowId"].ToString() != "")
            {
                model.PageRowId = int.Parse(row["PageRowId"].ToString());
            }
            if (row["BlogId"] != null && row["BlogId"].ToString() != "")
            {
                model.BlogId = new Guid(row["BlogId"].ToString());
            }
            if (row["PageId"] != null && row["PageId"].ToString() != "")
            {
                model.PageId = new Guid(row["PageId"].ToString());
            }
            if (row["Title"] != null)
            {
                model.Title = row["Title"].ToString();
            }
            if (row["Description"] != null)
            {
                model.Description = row["Description"].ToString();
            }
            if (row["PageContent"] != null)
            {
                model.PageContent = row["PageContent"].ToString();
            }
            if (row["Keywords"] != null)
            {
                model.Keywords = row["Keywords"].ToString();
            }
            if (row["DateCreated"] != null && row["DateCreated"].ToString() != "")
            {
                model.DateCreated = DateTime.Parse(row["DateCreated"].ToString());
            }
            if (row["DateModified"] != null && row["DateModified"].ToString() != "")
            {
                model.DateModified = DateTime.Parse(row["DateModified"].ToString());
            }
            if (row["IsPublished"] != null && row["IsPublished"].ToString() != "")
            {
                if ((row["IsPublished"].ToString() == "1") || (row["IsPublished"].ToString().ToLower() == "true"))
                {
                    model.IsPublished = true;
                }
                else
                {
                    model.IsPublished = false;
                }
            }
            if (row["IsFrontPage"] != null && row["IsFrontPage"].ToString() != "")
            {
                if ((row["IsFrontPage"].ToString() == "1") || (row["IsFrontPage"].ToString().ToLower() == "true"))
                {
                    model.IsFrontPage = true;
                }
                else
                {
                    model.IsFrontPage = false;
                }
            }
            if (row["Parent"] != null && row["Parent"].ToString() != "")
            {
                model.Parent = new Guid(row["Parent"].ToString());
            }
            if (row["ShowInList"] != null && row["ShowInList"].ToString() != "")
            {
                if ((row["ShowInList"].ToString() == "1") || (row["ShowInList"].ToString().ToLower() == "true"))
                {
                    model.ShowInList = true;
                }
                else
                {
                    model.ShowInList = false;
                }
            }
            if (row["Slug"] != null)
            {
                model.Slug = row["Slug"].ToString();
            }
            if (row["IsDeleted"] != null && row["IsDeleted"].ToString() != "")
            {
                if ((row["IsDeleted"].ToString() == "1") || (row["IsDeleted"].ToString().ToLower() == "true"))
                {
                    model.IsDeleted = true;
                }
                else
                {
                    model.IsDeleted = false;
                }
            }

            return model;
        }

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataTable GetList(string strWhere)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select PageRowId,BlogId,PageId,Title,Description,PageContent,Keywords,DateCreated,DateModified,IsPublished,IsFrontPage,Parent,ShowInList,Slug,IsDeleted ");
        //    strSql.Append(" FROM be_Pages ");
        //    if (strWhere.Trim() != "")
        //    {
        //        strSql.Append(" where " + strWhere);
        //    }
        //    return Helper.ExecuteTable(CommandType.Text, strSql.ToString());
        //}

    }
}

