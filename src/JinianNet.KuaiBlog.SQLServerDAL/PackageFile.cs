﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:PackageFiles
	/// </summary>
    public partial class PackageFile : BaseDAL
	{
		public PackageFile()
		{}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(JinianNet.KuaiBlog.Entity.PackageFiles model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into be_PackageFiles(");
			strSql.Append("PackageId,FileOrder,FilePath,IsDirectory)");
			strSql.Append(" values (");
			strSql.Append("@PackageId,@FileOrder,@FilePath,@IsDirectory)");
			SqlParameter[] parameters = {
					new SqlParameter("@PackageId", SqlDbType.NVarChar,128),
					new SqlParameter("@FileOrder", SqlDbType.Int,4),
					new SqlParameter("@FilePath", SqlDbType.NVarChar,255),
					new SqlParameter("@IsDirectory", SqlDbType.Bit,1)};
			parameters[0].Value = model.PackageId;
			parameters[1].Value = model.FileOrder;
			parameters[2].Value = model.FilePath;
			parameters[3].Value = model.IsDirectory;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.PackageFiles model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_PackageFiles set ");
			strSql.Append("FilePath=@FilePath,");
			strSql.Append("IsDirectory=@IsDirectory");
			strSql.Append(" where PackageId=@PackageId and FileOrder=@FileOrder ");
			SqlParameter[] parameters = {
					new SqlParameter("@FilePath", SqlDbType.NVarChar,255),
					new SqlParameter("@IsDirectory", SqlDbType.Bit,1),
					new SqlParameter("@PackageId", SqlDbType.NVarChar,128),
					new SqlParameter("@FileOrder", SqlDbType.Int,4)};
			parameters[0].Value = model.FilePath;
			parameters[1].Value = model.IsDirectory;
			parameters[2].Value = model.PackageId;
			parameters[3].Value = model.FileOrder;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string PackageId,int FileOrder)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_PackageFiles ");
			strSql.Append(" where PackageId=@PackageId and FileOrder=@FileOrder ");
			SqlParameter[] parameters = {
					new SqlParameter("@PackageId", SqlDbType.NVarChar,128),
					new SqlParameter("@FileOrder", SqlDbType.Int,4)			};
			parameters[0].Value = PackageId;
			parameters[1].Value = FileOrder;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.PackageFiles GetItem(string PackageId,int FileOrder)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 PackageId,FileOrder,FilePath,IsDirectory from be_PackageFiles ");
			strSql.Append(" where PackageId=@PackageId and FileOrder=@FileOrder ");
			SqlParameter[] parameters = {
					new SqlParameter("@PackageId", SqlDbType.NVarChar,128),
					new SqlParameter("@FileOrder", SqlDbType.Int,4)			};
			parameters[0].Value = PackageId;
			parameters[1].Value = FileOrder;

			JinianNet.KuaiBlog.Entity.PackageFiles model=new JinianNet.KuaiBlog.Entity.PackageFiles();
			DataTable dt=Helper.ExecuteTable(CommandType.Text,strSql.ToString(),parameters);
			if(dt.Rows.Count>0)
			{
				return DataRowToModel(dt.Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.PackageFiles DataRowToModel(DataRow row)
		{
			JinianNet.KuaiBlog.Entity.PackageFiles model=new JinianNet.KuaiBlog.Entity.PackageFiles();
			if (row != null)
			{
				if(row["PackageId"]!=null)
				{
					model.PackageId=row["PackageId"].ToString();
				}
				if(row["FileOrder"]!=null && row["FileOrder"].ToString()!="")
				{
					model.FileOrder=int.Parse(row["FileOrder"].ToString());
				}
				if(row["FilePath"]!=null)
				{
					model.FilePath=row["FilePath"].ToString();
				}
				if(row["IsDirectory"]!=null && row["IsDirectory"].ToString()!="")
				{
					if((row["IsDirectory"].ToString()=="1")||(row["IsDirectory"].ToString().ToLower()=="true"))
					{
						model.IsDirectory=true;
					}
					else
					{
						model.IsDirectory=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataTable GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select PackageId,FileOrder,FilePath,IsDirectory ");
			strSql.Append(" FROM be_PackageFiles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataTable GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" PackageId,FileOrder,FilePath,IsDirectory ");
			strSql.Append(" FROM be_PackageFiles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM be_PackageFiles ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.FileOrder desc");
			}
			strSql.Append(")AS Row, T.*  from be_PackageFiles T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "be_PackageFiles";
			parameters[1].Value = "FileOrder";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		

		
	}
}

