﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:PostTag
	/// </summary>
    public partial class PostTag : BaseDAL
	{
		public PostTag()
		{}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(JinianNet.KuaiBlog.Entity.PostTag model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into be_PostTag(");
			strSql.Append("BlogId,PostId,Tag)");
			strSql.Append(" values (");
			strSql.Append("@BlogId,@PostId,@Tag)");
			strSql.Append(";select @@IdENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Tag", SqlDbType.NVarChar,50)};
			parameters[0].Value = Guid.NewGuid();
			parameters[1].Value = Guid.NewGuid();
			parameters[2].Value = model.Tag;

			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.PostTag model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_PostTag set ");
			strSql.Append("BlogId=@BlogId,");
			strSql.Append("PostId=@PostId,");
			strSql.Append("Tag=@Tag");
			strSql.Append(" where PostTagId=@PostTagId");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PostId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Tag", SqlDbType.NVarChar,50),
					new SqlParameter("@PostTagId", SqlDbType.Int,4)};
			parameters[0].Value = model.BlogId;
			parameters[1].Value = model.PostId;
			parameters[2].Value = model.Tag;
			parameters[3].Value = model.PostTagId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int PostTagId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_PostTag ");
			strSql.Append(" where PostTagId=@PostTagId");
			SqlParameter[] parameters = {
					new SqlParameter("@PostTagId", SqlDbType.Int,4)
			};
			parameters[0].Value = PostTagId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string PostTagIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_PostTag ");
			strSql.Append(" where PostTagId in ("+PostTagIdlist + ")  ");
			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.PostTag GetItem(int PostTagId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 PostTagId,BlogId,PostId,Tag from be_PostTag ");
			strSql.Append(" where PostTagId=@PostTagId");
			SqlParameter[] parameters = {
					new SqlParameter("@PostTagId", SqlDbType.Int,4)
			};
			parameters[0].Value = PostTagId;

            using (System.Data.Common.DbDataReader dr = Helper.ExecuteReader(CommandType.Text, strSql.ToString(), parameters))
            {
                if (dr.Read())
                {
                    return GetItem(dr);
                }
            }
            return null;
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        private JinianNet.KuaiBlog.Entity.PostTag GetItem(IDataReader row)
		{
			JinianNet.KuaiBlog.Entity.PostTag model=new JinianNet.KuaiBlog.Entity.PostTag();

				if(row["PostTagId"]!=null && row["PostTagId"].ToString()!="")
				{
					model.PostTagId=int.Parse(row["PostTagId"].ToString());
				}
				if(row["BlogId"]!=null && row["BlogId"].ToString()!="")
				{
					model.BlogId= new Guid(row["BlogId"].ToString());
				}
				if(row["PostId"]!=null && row["PostId"].ToString()!="")
				{
					model.PostId= new Guid(row["PostId"].ToString());
				}
				if(row["Tag"]!=null)
				{
					model.Tag=row["Tag"].ToString();
				}

			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
        public IList<Entity.PostTag> GetList(Guid postId)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select PostTagId,BlogId,PostId,Tag ");
            strSql.Append(" FROM be_PostTag where PostId=@PostId");


            SqlParameter[] parameters = {
					new SqlParameter("@PostId", SqlDbType.UniqueIdentifier,16)};
            parameters[0].Value = postId;

            List<JinianNet.KuaiBlog.Entity.PostTag> list = new List<Entity.PostTag>();

            using (System.Data.Common.DbDataReader dr = Helper.ExecuteReader(CommandType.Text, strSql.ToString(), parameters))
            {
                while (dr.Read())
                {
                    list.Add(GetItem(dr));
                }
            }
            return list;
		}

        ///// <summary>
        ///// 获得前几行数据
        ///// </summary>
        //public DataTable GetTagList(Guid blogId)
        //{

        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select CategoryRowId,BlogId,CategoryId,CategoryName,Description,ParentId,Slug from be_Categories where BlogId=@BlogId");

        //    SqlParameter[] parameters = {
        //            new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16)};
        //    parameters[0].Value = blogId;

        //    return Helper.ExecuteTable(CommandType.Text, "select Tag,");
        //}

		
	}
}

