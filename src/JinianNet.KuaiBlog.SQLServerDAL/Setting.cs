﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Settings
    /// </summary>
    public partial class Setting : BaseDAL
    {
        public Setting()
        { }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(JinianNet.KuaiBlog.Entity.Setting model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into be_Settings(");
            strSql.Append("BlogId,SettingName,SettingValue)");
            strSql.Append(" values (");
            strSql.Append("@BlogId,@SettingName,@SettingValue)");
            strSql.Append(";select @@IdENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SettingName", SqlDbType.NVarChar,50),
					new SqlParameter("@SettingValue", SqlDbType.NVarChar,-1)};
            parameters[0].Value = Guid.NewGuid();
            parameters[1].Value = model.SettingName;
            parameters[2].Value = model.SettingValue;

            object obj = Helper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(JinianNet.KuaiBlog.Entity.Setting model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update be_Settings set ");
            strSql.Append("BlogId=@BlogId,");
            strSql.Append("SettingName=@SettingName,");
            strSql.Append("SettingValue=@SettingValue");
            strSql.Append(" where SettingRowId=@SettingRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SettingName", SqlDbType.NVarChar,50),
					new SqlParameter("@SettingValue", SqlDbType.NVarChar,-1),
					new SqlParameter("@SettingRowId", SqlDbType.Int,4)};
            parameters[0].Value = model.BlogId;
            parameters[1].Value = model.SettingName;
            parameters[2].Value = model.SettingValue;
            parameters[3].Value = model.SettingRowId;

            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SettingRowId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from be_Settings ");
            strSql.Append(" where SettingRowId=@SettingRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@SettingRowId", SqlDbType.Int,4)
			};
            parameters[0].Value = SettingRowId;

            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SettingRowIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from be_Settings ");
            strSql.Append(" where SettingRowId in (" + SettingRowIdlist + ")  ");
            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public JinianNet.KuaiBlog.Entity.Setting GetItem(int SettingRowId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SettingRowId,BlogId,SettingName,SettingValue from be_Settings ");
            strSql.Append(" where SettingRowId=@SettingRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@SettingRowId", SqlDbType.Int,4)
			};
            parameters[0].Value = SettingRowId;

            using (System.Data.Common.DbDataReader dr = Helper.ExecuteReader(CommandType.Text, strSql.ToString(), parameters))
            {
                if (dr.Read())
                {
                    return GetItem(dr);
                }
            }
            return null;
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public JinianNet.KuaiBlog.Entity.Setting GetItem(System.Data.Common.DbDataReader rd)
        {
            JinianNet.KuaiBlog.Entity.Setting model = new JinianNet.KuaiBlog.Entity.Setting();

            if (rd["SettingRowId"] != null && rd["SettingRowId"].ToString() != "")
            {
                model.SettingRowId = int.Parse(rd["SettingRowId"].ToString());
            }
            if (rd["BlogId"] != null && rd["BlogId"].ToString() != "")
            {
                model.BlogId = new Guid(rd["BlogId"].ToString());
            }
            if (rd["SettingName"] != null)
            {
                model.SettingName = rd["SettingName"].ToString();
            }
            if (rd["SettingValue"] != null)
            {
                model.SettingValue = rd["SettingValue"].ToString();
            }

            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<JinianNet.KuaiBlog.Entity.Setting> GetList(Guid blogId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SettingRowId,BlogId,SettingName,SettingValue ");
            strSql.Append(" FROM be_Settings where BlogId=@BlogId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16)};
            parameters[0].Value = blogId;

            List<JinianNet.KuaiBlog.Entity.Setting> list = new List<Entity.Setting>();

            using (System.Data.Common.DbDataReader dr = Helper.ExecuteReader(CommandType.Text, strSql.ToString(), parameters))
            {
                while (dr.Read())
                {
                    list.Add(GetItem(dr));
                }
            }
            return list;

        }
    }
}

