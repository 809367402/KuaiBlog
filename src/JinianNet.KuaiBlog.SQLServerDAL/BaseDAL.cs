﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
    public class BaseDAL
    {
        private SqlHelper _sqlhelper;
        public BaseDAL()
        {
            _sqlhelper = new SqlHelper();
        }

        public SqlHelper Helper
        {
            get
            {
                return _sqlhelper;
            } 
        }
    }
}
