﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
	/// <summary>
	/// 数据访问类:Referrers
	/// </summary>
    public partial class Referrer : BaseDAL
	{
		public Referrer()
		{}
		


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(JinianNet.KuaiBlog.Entity.Referrer model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into be_Referrers(");
			strSql.Append("BlogId,ReferrerId,ReferralDay,ReferrerUrl,ReferralCount,Url,IsSpam)");
			strSql.Append(" values (");
			strSql.Append("@BlogId,@ReferrerId,@ReferralDay,@ReferrerUrl,@ReferralCount,@Url,@IsSpam)");
			strSql.Append(";select @@IdENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@ReferrerId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@ReferralDay", SqlDbType.DateTime),
					new SqlParameter("@ReferrerUrl", SqlDbType.VarChar,255),
					new SqlParameter("@ReferralCount", SqlDbType.Int,4),
					new SqlParameter("@Url", SqlDbType.VarChar,255),
					new SqlParameter("@IsSpam", SqlDbType.Bit,1)};
			parameters[0].Value = Guid.NewGuid();
			parameters[1].Value = Guid.NewGuid();
			parameters[2].Value = model.ReferralDay;
			parameters[3].Value = model.ReferrerUrl;
			parameters[4].Value = model.ReferralCount;
			parameters[5].Value = model.Url;
			parameters[6].Value = model.IsSpam;

			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(JinianNet.KuaiBlog.Entity.Referrer model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update be_Referrers set ");
			strSql.Append("BlogId=@BlogId,");
			strSql.Append("ReferrerId=@ReferrerId,");
			strSql.Append("ReferralDay=@ReferralDay,");
			strSql.Append("ReferrerUrl=@ReferrerUrl,");
			strSql.Append("ReferralCount=@ReferralCount,");
			strSql.Append("Url=@Url,");
			strSql.Append("IsSpam=@IsSpam");
			strSql.Append(" where ReferrerRowId=@ReferrerRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@ReferrerId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@ReferralDay", SqlDbType.DateTime),
					new SqlParameter("@ReferrerUrl", SqlDbType.VarChar,255),
					new SqlParameter("@ReferralCount", SqlDbType.Int,4),
					new SqlParameter("@Url", SqlDbType.VarChar,255),
					new SqlParameter("@IsSpam", SqlDbType.Bit,1),
					new SqlParameter("@ReferrerRowId", SqlDbType.Int,4)};
			parameters[0].Value = model.BlogId;
			parameters[1].Value = model.ReferrerId;
			parameters[2].Value = model.ReferralDay;
			parameters[3].Value = model.ReferrerUrl;
			parameters[4].Value = model.ReferralCount;
			parameters[5].Value = model.Url;
			parameters[6].Value = model.IsSpam;
			parameters[7].Value = model.ReferrerRowId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ReferrerRowId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_Referrers ");
			strSql.Append(" where ReferrerRowId=@ReferrerRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@ReferrerRowId", SqlDbType.Int,4)
			};
			parameters[0].Value = ReferrerRowId;

			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string ReferrerRowIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from be_Referrers ");
			strSql.Append(" where ReferrerRowId in ("+ReferrerRowIdlist + ")  ");
			int rows=Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.Referrer GetItem(int ReferrerRowId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ReferrerRowId,BlogId,ReferrerId,ReferralDay,ReferrerUrl,ReferralCount,Url,IsSpam from be_Referrers ");
			strSql.Append(" where ReferrerRowId=@ReferrerRowId");
			SqlParameter[] parameters = {
					new SqlParameter("@ReferrerRowId", SqlDbType.Int,4)
			};
			parameters[0].Value = ReferrerRowId;

			JinianNet.KuaiBlog.Entity.Referrer model=new JinianNet.KuaiBlog.Entity.Referrer();
			DataTable dt=Helper.ExecuteTable(CommandType.Text,strSql.ToString(),parameters);
			if(dt.Rows.Count>0)
			{
				return DataRowToModel(dt.Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public JinianNet.KuaiBlog.Entity.Referrer DataRowToModel(DataRow row)
		{
			JinianNet.KuaiBlog.Entity.Referrer model=new JinianNet.KuaiBlog.Entity.Referrer();
			if (row != null)
			{
				if(row["ReferrerRowId"]!=null && row["ReferrerRowId"].ToString()!="")
				{
					model.ReferrerRowId=int.Parse(row["ReferrerRowId"].ToString());
				}
				if(row["BlogId"]!=null && row["BlogId"].ToString()!="")
				{
					model.BlogId= new Guid(row["BlogId"].ToString());
				}
				if(row["ReferrerId"]!=null && row["ReferrerId"].ToString()!="")
				{
					model.ReferrerId= new Guid(row["ReferrerId"].ToString());
				}
				if(row["ReferralDay"]!=null && row["ReferralDay"].ToString()!="")
				{
					model.ReferralDay=DateTime.Parse(row["ReferralDay"].ToString());
				}
				if(row["ReferrerUrl"]!=null)
				{
					model.ReferrerUrl=row["ReferrerUrl"].ToString();
				}
				if(row["ReferralCount"]!=null && row["ReferralCount"].ToString()!="")
				{
					model.ReferralCount=int.Parse(row["ReferralCount"].ToString());
				}
				if(row["Url"]!=null)
				{
					model.Url=row["Url"].ToString();
				}
				if(row["IsSpam"]!=null && row["IsSpam"].ToString()!="")
				{
					if((row["IsSpam"].ToString()=="1")||(row["IsSpam"].ToString().ToLower()=="true"))
					{
						model.IsSpam=true;
					}
					else
					{
						model.IsSpam=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataTable GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ReferrerRowId,BlogId,ReferrerId,ReferralDay,ReferrerUrl,ReferralCount,Url,IsSpam ");
			strSql.Append(" FROM be_Referrers ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataTable GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ReferrerRowId,BlogId,ReferrerId,ReferralDay,ReferrerUrl,ReferralCount,Url,IsSpam ");
			strSql.Append(" FROM be_Referrers ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM be_Referrers ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ReferrerRowId desc");
			}
			strSql.Append(")AS Row, T.*  from be_Referrers T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataTable GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "be_Referrers";
			parameters[1].Value = "ReferrerRowId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		

		
	}
}

