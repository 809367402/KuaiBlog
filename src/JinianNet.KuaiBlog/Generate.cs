﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace JinianNet.KuaiBlog
{
    public class Generate
    {
        System.Web.HttpContext context;

        public Generate(System.Web.HttpContext  ctx)
        {
            this.context = ctx;
        }

        public void GeneratePost(string slug)
        {
            Entity.Post model = new SQLServerDAL.Post().GetItem(slug);
            if (model == null)
            {
                this.context.Response.StatusCode = 404;
                return;
            }
            JNTemplate.ITemplate template = GetTemplate("post");
            template.Context.TempData["post"] = model;
            template.Context.TempData["categories"] = new SQLServerDAL.Category().GetPostCategories(model.PostId);
            template.Render(this.context.Response.Output);
        }

        public void GeneratePostList(string slug)
        {
            Entity.Blog blog = Blog.CurrentBlog(this.context.Request.Url.Host);
            if (blog == null)
            {
                this.context.Response.StatusCode = 404;
                return;
            }

            JNTemplate.ITemplate template = GetTemplate("channel");
   
            template.Context.TempData["category"] = new SQLServerDAL.Category().GetItem(slug);

            template.Render(this.context.Response.Output);
        }

        private JNTemplate.ITemplate GetTemplate(string name)
        {
            string path = string.Concat(HttpRuntime.AppDomainAppPath, System.IO.Path.DirectorySeparatorChar, "themes", System.IO.Path.DirectorySeparatorChar, "default", System.IO.Path.DirectorySeparatorChar, name, ".html");

            JNTemplate.ITemplate t = JinianNet.JNTemplate.BuildManager.CreateTemplate(path);
            t.Context.TempData["Sys"] = new JinianNet.KuaiBlog.TemplateFunc();

            var list = new SQLServerDAL.Setting().GetList(Blog.CurrentBlog(context.Request.Url.Host).BlogId);

            Dictionary<string, string> blog = new Dictionary<string, string>( StringComparer.OrdinalIgnoreCase);
            blog["host"] = context.Request.Url.Host;
            for (var i = 0; i < list.Count;i++ )
            {
                blog[list[i].SettingName] = list[i].SettingValue;
            }
            t.Context.TempData["Blog"] = blog;
            t.Context.TempData["Now"] = DateTime.Now;
            return t;
        }

        public void GetDefault()
        {
            JNTemplate.ITemplate template = GetTemplate("index");
            template.Render(this.context.Response.Output);
        }

        public void GeneratePage(string slug)
        {
            Entity.Page model = new SQLServerDAL.Page().GetItem(slug);
            if (model == null)
            {
                this.context.Response.StatusCode = 404;
                return;
            }
            JNTemplate.ITemplate template = GetTemplate("page");
            template.Context.TempData["model"] = model;
            template.Render(this.context.Response.Output);
        }
    }
}
