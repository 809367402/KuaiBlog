﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JinianNet.KuaiBlog
{
    public class PageDataSource<T>
    {
        public IList<T> DataSource
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public int PageCount
        {
            get;
            set;
        }

        /// <summary>
        /// 分页url，如/home/index.aspx?page={index}
        /// </summary>
        public string HomePageUrl
        {
            set;
            get;
        }

        /// <summary>
        /// 分页url，如/home/index.aspx?page={index}
        /// </summary>
        public string PageUrl
        {
            set;
            get;
        }

        public string ToPageString()
        {
            int step = 7;

            StringBuilder sbr = new StringBuilder();
            if (PageIndex < 2)
            {
                sbr.Append("<span>&lt;&lt;</span> <span>&lt;</span> ");
            }
            else
            {
                sbr.Append("<a href=\"");
                sbr.Append(HomePageUrl);
                sbr.Append("\">&lt;&lt;</a> ");
                sbr.Append("<a href=\"");
                if (PageIndex == 2)
                {
                    sbr.Append(HomePageUrl);
                }
                else
                {
                    sbr.Append(PageUrl.Replace("{index}", (PageIndex - 1).ToString()));
                }
                sbr.Append("\">&lt;</a> ");
            }


            int start = PageIndex - ((step - 1) / 2);
            int temp;

            if (start < 1)
            {
                start = 1;
            }

            for (int i = 0; i < step; i++)
            {
                temp = start + i;

                if (temp > PageCount)
                {
                    break;
                }
                if (temp == PageIndex)
                {
                    sbr.Append("<span>");
                    sbr.Append(temp.ToString());
                    sbr.Append("</span> ");
                }
                else
                {
                    sbr.Append("<a href=\"");
                    sbr.Append(PageUrl.Replace("{index}", temp.ToString()));
                    sbr.Append("\">");
                    sbr.Append(temp.ToString());
                    sbr.Append("</a> ");
                }
            }


            if (PageIndex >= PageCount)
            {
                sbr.Append("<span>&gt;</span> <span>&gt;&gt;</span>");
            }
            else
            {
                sbr.Append("<a href=\"");
                sbr.Append(PageUrl.Replace("{index}", (PageIndex + 1).ToString()));
                sbr.Append("\">&gt;</a> ");

                sbr.Append("<a href=\"");
                sbr.Append(PageUrl.Replace("{index}", PageCount.ToString()));
                sbr.Append("\">&gt;&gt;</a>");
            }
            return sbr.ToString();
        }
    }
}
