﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace JinianNet.KuaiBlog
{
    public class TemplateFunc
    {
        public string GetUrl(object value)
        {
            Type t = value.GetType();

            switch (t.FullName)
            {
                case "JinianNet.KuaiBlog.Entity.Post":
                    return GetPostUrl((Entity.Post)value);
                case "JinianNet.KuaiBlog.Entity.Category":
                    return GetCategoryUrl((Entity.Category)value);
                case "JinianNet.KuaiBlog.Entity.Page":
                    return GetCategoryUrl((Entity.Category)value);
                case "JinianNet.KuaiBlog.Entity.PostTag":
                    return GetTagUrl((Entity.PostTag)value);
            }

            return null;
        }

        public IList<Entity.Category> GetCategoryList()
        {
            return new SQLServerDAL.Category().GetList(Blog.CurrentBlog(System.Web.HttpContext.Current.Request.Url.Host).BlogId);
        }

        public PageDataSource<Entity.Post> GetDefaultPostList()
        {
            System.Web.HttpRequest Request = System.Web.HttpContext.Current.Request;

            int page;
            if (string.IsNullOrEmpty(Request.QueryString["page"]) || !int.TryParse(Request.QueryString["page"], out page) || page < 1)
                page = 1;
            int pageCount;

            string tag =(Request.QueryString["tag"] ?? "").Trim('/');

            IList<Entity.Post> list = new SQLServerDAL.Post().GetList(Blog.CurrentBlog(Request.Url.Host).BlogId,
                null, tag, null, page, 10, out pageCount);

            PageDataSource<Entity.Post> data = new PageDataSource<Entity.Post>();

            if (string.IsNullOrEmpty(tag) || tag.Trim().Length == 0)
            {
                data.HomePageUrl = "/";
                data.PageUrl = string.Concat(data.HomePageUrl, "?page={index}");
            }
            else
            {
                data.HomePageUrl = string.Concat("/?tag=/", System.Web.HttpUtility.UrlEncode(tag));
                data.PageUrl = string.Concat(data.HomePageUrl, "&page={index}");
            }

            data.DataSource = list;
            data.PageIndex = page;
            data.PageSize = 10;
            data.PageCount = pageCount;

            return data;
        }

        public PageDataSource<Entity.Post> GetPostList(string slug)
        {
            System.Web.HttpRequest Request= System.Web.HttpContext.Current.Request ;

            int page;
            if (string.IsNullOrEmpty(Request.QueryString["page"]) || !int.TryParse(Request.QueryString["page"], out page) || page < 1)
                page = 1;
            int pageCount;

            IList<Entity.Post> list = new SQLServerDAL.Post().GetList(Blog.CurrentBlog(Request.Url.Host).BlogId,
                slug, page, 10,out pageCount);

            PageDataSource<Entity.Post> data = new PageDataSource<Entity.Post>();
            data.HomePageUrl = System.Web.HttpContext.Current.Request.FilePath;
            data.PageUrl = string.Concat( data.HomePageUrl,"?page={index}");
            data.DataSource = list;
            data.PageIndex = page;
            data.PageSize = 10;
            data.PageCount = pageCount;
            return data;
        }

        public IList<Entity.PostTag> GetPostTagList(Guid postId)
        {
            return new SQLServerDAL.PostTag().GetList(postId);
        }

        //public IList<Entity.PostTag> GetTagList()
        //{

        //}
        private string GetPostUrl(Entity.Post value)
        {
            if (value == null)
                return null;
            return string.Concat(
                        "/post/",
                        value.DateCreated.ToString("yyyy\\/MM\\/dd\\/"),
                        System.Web.HttpUtility.UrlEncode(value.Slug),
                        ".aspx");
        }

        private string GetCategoryUrl(Entity.Category value) {
            if (value == null)
                return null;
            return string.Concat(
                        "/category/",
                        System.Web.HttpUtility.UrlEncode(value.Slug),
                        ".aspx");
        }

        private string GetPageUrl(Entity.Page value)
        {
            if (value == null)
                return null;
            return string.Concat(
                        "/Page/",
                        System.Web.HttpUtility.UrlEncode(value.Slug),
                        ".aspx");
        }

        private string GetTagUrl(Entity.PostTag value)
        {
            if (value == null)
                return null;
            return string.Concat(
                        "/?tag=/",
                        System.Web.HttpUtility.UrlEncode(value.Tag));
        }


    }
}
