/****** Object:  Table [be_PingService]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_PingService](
	[PingServiceID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[Link] [nvarchar](255) NULL,
 CONSTRAINT [PK_be_PingService_1] PRIMARY KEY CLUSTERED 
(
	[PingServiceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_PingService_BlogId] ON [be_PingService] 
(
	[BlogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Pages]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Pages](
	[PageRowID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[PageID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[PageContent] [nvarchar](max) NULL,
	[Keywords] [nvarchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[IsPublished] [bit] NULL,
	[IsFrontPage] [bit] NULL,
	[Parent] [uniqueidentifier] NULL,
	[ShowInList] [bit] NULL,
	[Slug] [nvarchar](255) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_be_Pages] PRIMARY KEY CLUSTERED 
(
	[PageRowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_Pages_BlogId_PageId] ON [be_Pages] 
(
	[BlogID] ASC,
	[PageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Packages]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Packages](
	[PackageId] [nvarchar](128) NOT NULL,
	[Version] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_be_Packages] PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [be_PackageFiles]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_PackageFiles](
	[PackageId] [nvarchar](128) NOT NULL,
	[FileOrder] [int] NOT NULL,
	[FilePath] [nvarchar](255) NOT NULL,
	[IsDirectory] [bit] NOT NULL,
 CONSTRAINT [PK_be_PackageFiles] PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC,
	[FileOrder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [be_Users]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[LastLoginTime] [datetime] NULL,
	[EmailAddress] [nvarchar](100) NULL,
 CONSTRAINT [PK_be_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_Users_BlogId_UserName] ON [be_Users] 
(
	[BlogID] ASC,
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_UserRoles]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_UserRoles](
	[UserRoleID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[Role] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_be_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserRoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_UserRoles_BlogId] ON [be_UserRoles] 
(
	[BlogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_StopWords]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_StopWords](
	[StopWordRowId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[StopWord] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_be_StopWords] PRIMARY KEY CLUSTERED 
(
	[StopWordRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_StopWords_BlogId] ON [be_StopWords] 
(
	[BlogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Settings]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Settings](
	[SettingRowId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[SettingName] [nvarchar](50) NOT NULL,
	[SettingValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_be_Settings] PRIMARY KEY CLUSTERED 
(
	[SettingRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_Settings_BlogId] ON [be_Settings] 
(
	[BlogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_be_Settings] ON [be_Settings] 
(
	[BlogId] ASC,
	[SettingName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Roles]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[Role] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_be_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_be_Roles_BlogID_Role] ON [be_Roles] 
(
	[BlogID] ASC,
	[Role] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Rights]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Rights](
	[RightRowId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[RightName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_be_Rights] PRIMARY KEY CLUSTERED 
(
	[RightRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_Rights_BlogId] ON [be_Rights] 
(
	[BlogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_RightRoles]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_RightRoles](
	[RightRoleRowId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[RightName] [nvarchar](100) NOT NULL,
	[Role] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_be_RightRoles] PRIMARY KEY CLUSTERED 
(
	[RightRoleRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_RightRoles_BlogId] ON [be_RightRoles] 
(
	[BlogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Referrers]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [be_Referrers](
	[ReferrerRowId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[ReferrerId] [uniqueidentifier] NOT NULL,
	[ReferralDay] [datetime] NOT NULL,
	[ReferrerUrl] [varchar](255) NOT NULL,
	[ReferralCount] [int] NOT NULL,
	[Url] [varchar](255) NULL,
	[IsSpam] [bit] NULL,
 CONSTRAINT [PK_be_Referrers] PRIMARY KEY CLUSTERED 
(
	[ReferrerRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [idx_be_Referrers_BlogId] ON [be_Referrers] 
(
	[BlogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_QuickSettings]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_QuickSettings](
	[QuickSettingID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[SettingName] [nvarchar](255) NOT NULL,
	[SettingValue] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_be_QuickSettings] PRIMARY KEY CLUSTERED 
(
	[QuickSettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [be_QuickNotes]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_QuickNotes](
	[QuickNoteID] [int] IDENTITY(1,1) NOT NULL,
	[NoteID] [uniqueidentifier] NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Updated] [datetime] NULL,
 CONSTRAINT [PK_be_QuickNotes] PRIMARY KEY CLUSTERED 
(
	[QuickNoteID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_NoteId_BlogId_UserName] ON [be_QuickNotes] 
(
	[NoteID] ASC,
	[BlogID] ASC,
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Profiles]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Profiles](
	[ProfileID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[SettingName] [nvarchar](200) NULL,
	[SettingValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_be_Profiles] PRIMARY KEY CLUSTERED 
(
	[ProfileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_Profiles_BlogId_UserName] ON [be_Profiles] 
(
	[BlogID] ASC,
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_FileStoreDirectory]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [be_FileStoreDirectory](
	[Id] [uniqueidentifier] NOT NULL,
	[ParentID] [uniqueidentifier] NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[FullPath] [varchar](1000) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastAccess] [datetime] NOT NULL,
	[LastModify] [datetime] NOT NULL,
 CONSTRAINT [PK_be_FileStoreDirectory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [be_DataStoreSettings]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_DataStoreSettings](
	[DataStoreSettingRowId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[ExtensionType] [nvarchar](50) NOT NULL,
	[ExtensionId] [nvarchar](100) NOT NULL,
	[Settings] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_be_DataStoreSettings] PRIMARY KEY CLUSTERED 
(
	[DataStoreSettingRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_DataStoreSettings_BlogId_ExtensionType_TypeID] ON [be_DataStoreSettings] 
(
	[BlogId] ASC,
	[ExtensionType] ASC,
	[ExtensionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_CustomFields]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_CustomFields](
	[CustomType] [nvarchar](25) NOT NULL,
	[ObjectId] [nvarchar](100) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[Key] [nvarchar](150) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[Attribute] [nvarchar](250) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_CustomType_ObjectId_BlogId_Key] ON [be_CustomFields] 
(
	[CustomType] ASC,
	[ObjectId] ASC,
	[BlogId] ASC,
	[Key] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Categories]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Categories](
	[CategoryRowID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[CategoryID] [uniqueidentifier] NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[Description] [nvarchar](200) NULL,
	[ParentID] [uniqueidentifier] NULL,
	[Slug] [nvarchar](255) NULL,
 CONSTRAINT [PK_be_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryRowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_be_Categories_BlogID_CategoryID] ON [be_Categories] 
(
	[BlogID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Blogs]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Blogs](
	[BlogRowId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[BlogName] [nvarchar](255) NOT NULL,
	[Hostname] [nvarchar](255) NOT NULL,
	[IsAnyTextBeforeHostnameAccepted] [bit] NOT NULL,
	[StorageContainerName] [nvarchar](255) NOT NULL,
	[VirtualPath] [nvarchar](255) NOT NULL,
	[IsPrimary] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsSiteAggregation] [bit] NOT NULL,
 CONSTRAINT [PK_be_Blogs] PRIMARY KEY CLUSTERED 
(
	[BlogRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [be_BlogRollItems]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [be_BlogRollItems](
	[BlogRollRowId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[BlogRollId] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[BlogUrl] [varchar](255) NOT NULL,
	[FeedUrl] [varchar](255) NULL,
	[Xfn] [varchar](255) NULL,
	[SortIndex] [int] NOT NULL,
 CONSTRAINT [PK_be_BlogRollItems] PRIMARY KEY CLUSTERED 
(
	[BlogRollRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [idx_be_BlogRollItems_BlogId] ON [be_BlogRollItems] 
(
	[BlogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_Posts]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_Posts](
	[PostRowID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[PostID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[PostContent] [nvarchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[Author] [nvarchar](50) NULL,
	[IsPublished] [bit] NULL,
	[IsCommentEnabled] [bit] NULL,
	[Raters] [int] NULL,
	[Rating] [real] NULL,
	[Slug] [nvarchar](255) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_be_Posts] PRIMARY KEY CLUSTERED 
(
	[PostRowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [be_Posts_BlogID_PostID] ON [be_Posts] 
(
	[BlogID] ASC,
	[PostID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_PostNotify]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_PostNotify](
	[PostNotifyID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[PostID] [uniqueidentifier] NOT NULL,
	[NotifyAddress] [nvarchar](255) NULL,
 CONSTRAINT [idx_be_PostCategory_BlogId_PostId] PRIMARY KEY CLUSTERED 
(
	[PostNotifyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FK_PostID] ON [be_PostNotify] 
(
	[BlogID] ASC,
	[PostID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_PostComment]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_PostComment](
	[PostCommentRowID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[PostCommentID] [uniqueidentifier] NOT NULL,
	[PostID] [uniqueidentifier] NOT NULL,
	[ParentCommentID] [uniqueidentifier] NOT NULL,
	[CommentDate] [datetime] NOT NULL,
	[Author] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Website] [nvarchar](255) NULL,
	[Comment] [nvarchar](max) NULL,
	[Country] [nvarchar](255) NULL,
	[Ip] [nvarchar](50) NULL,
	[IsApproved] [bit] NULL,
	[ModeratedBy] [nvarchar](100) NULL,
	[Avatar] [nvarchar](255) NULL,
	[IsSpam] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_be_PostComment] PRIMARY KEY CLUSTERED 
(
	[PostCommentRowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_PostComment_BlogId_PostId] ON [be_PostComment] 
(
	[BlogID] ASC,
	[PostID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_PostCategory]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_PostCategory](
	[PostCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[PostID] [uniqueidentifier] NOT NULL,
	[CategoryID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_be_PostCategory_1] PRIMARY KEY CLUSTERED 
(
	[PostCategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_PostCategory_BlogId_CategoryId] ON [be_PostCategory] 
(
	[BlogID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_PostCategory_BlogId_PostId] ON [be_PostCategory] 
(
	[BlogID] ASC,
	[PostID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_PostTag]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [be_PostTag](
	[PostTagID] [int] IDENTITY(1,1) NOT NULL,
	[BlogID] [uniqueidentifier] NOT NULL,
	[PostID] [uniqueidentifier] NOT NULL,
	[Tag] [nvarchar](50) NULL,
 CONSTRAINT [PK_be_PostTag] PRIMARY KEY CLUSTERED 
(
	[PostTagID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_be_PostTag_BlogId_PostId] ON [be_PostTag] 
(
	[BlogID] ASC,
	[PostID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [be_FileStoreFiles]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [be_FileStoreFiles](
	[FileID] [uniqueidentifier] NOT NULL,
	[ParentDirectoryID] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[FullPath] [varchar](255) NOT NULL,
	[Contents] [varbinary](max) NOT NULL,
	[Size] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastAccess] [datetime] NOT NULL,
	[LastModify] [datetime] NOT NULL,
 CONSTRAINT [PK_be_FileStoreFiles] PRIMARY KEY CLUSTERED 
(
	[FileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [be_FileStoreFileThumbs]    Script Date: 04/07/2015 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [be_FileStoreFileThumbs](
	[thumbnailId] [uniqueidentifier] NOT NULL,
	[FileId] [uniqueidentifier] NOT NULL,
	[size] [int] NOT NULL,
	[contents] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_be_FileStoreFileThumbs] PRIMARY KEY CLUSTERED 
(
	[thumbnailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__be_Pages__PageID__0F975522]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_Pages] ADD  DEFAULT (newid()) FOR [PageID]
GO
/****** Object:  Default [DF__be_Pages__IsDele__0425A276]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_Pages] ADD  CONSTRAINT [DF__be_Pages__IsDele__0425A276]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_be_Referrers_Day]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_Referrers] ADD  CONSTRAINT [DF_be_Referrers_Day]  DEFAULT (getdate()) FOR [ReferralDay]
GO
/****** Object:  Default [DF__be_Catego__Categ__014935CB]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_Categories] ADD  DEFAULT (newid()) FOR [CategoryID]
GO
/****** Object:  Default [DF_be_Blogs_IsSiteAggregation]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_Blogs] ADD  CONSTRAINT [DF_be_Blogs_IsSiteAggregation]  DEFAULT ((0)) FOR [IsSiteAggregation]
GO
/****** Object:  Default [DF__be_Posts__PostID__1B0907CE]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_Posts] ADD  DEFAULT (newid()) FOR [PostID]
GO
/****** Object:  Default [DF__be_Posts__IsDele__09DE7BCC]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_Posts] ADD  CONSTRAINT [DF__be_Posts__IsDele__09DE7BCC]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__be_PostCo__PostC__164452B1]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostComment] ADD  DEFAULT (newid()) FOR [PostCommentID]
GO
/****** Object:  Default [DF__be_PostCo__Paren__173876EA]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostComment] ADD  CONSTRAINT [DF__be_PostCo__Paren__173876EA]  DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [ParentCommentID]
GO
/****** Object:  Default [DF__be_PostCo__IsSpa__182C9B23]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostComment] ADD  CONSTRAINT [DF__be_PostCo__IsSpa__182C9B23]  DEFAULT ((0)) FOR [IsSpam]
GO
/****** Object:  Default [DF__be_PostCo__IsDel__1920BF5C]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostComment] ADD  CONSTRAINT [DF__be_PostCo__IsDel__1920BF5C]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  ForeignKey [FK_be_PostNotify_be_Posts]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostNotify]  WITH CHECK ADD  CONSTRAINT [FK_be_PostNotify_be_Posts] FOREIGN KEY([BlogID], [PostID])
REFERENCES [be_Posts] ([BlogID], [PostID])
GO
ALTER TABLE [be_PostNotify] CHECK CONSTRAINT [FK_be_PostNotify_be_Posts]
GO
/****** Object:  ForeignKey [FK_be_PostComment_be_Posts]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostComment]  WITH CHECK ADD  CONSTRAINT [FK_be_PostComment_be_Posts] FOREIGN KEY([BlogID], [PostID])
REFERENCES [be_Posts] ([BlogID], [PostID])
GO
ALTER TABLE [be_PostComment] CHECK CONSTRAINT [FK_be_PostComment_be_Posts]
GO
/****** Object:  ForeignKey [FK_be_PostCategory_be_Categories]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostCategory]  WITH CHECK ADD  CONSTRAINT [FK_be_PostCategory_be_Categories] FOREIGN KEY([BlogID], [CategoryID])
REFERENCES [be_Categories] ([BlogID], [CategoryID])
GO
ALTER TABLE [be_PostCategory] CHECK CONSTRAINT [FK_be_PostCategory_be_Categories]
GO
/****** Object:  ForeignKey [FK_be_PostCategory_be_Posts]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostCategory]  WITH CHECK ADD  CONSTRAINT [FK_be_PostCategory_be_Posts] FOREIGN KEY([BlogID], [PostID])
REFERENCES [be_Posts] ([BlogID], [PostID])
GO
ALTER TABLE [be_PostCategory] CHECK CONSTRAINT [FK_be_PostCategory_be_Posts]
GO
/****** Object:  ForeignKey [FK_be_PostTag_be_Posts]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_PostTag]  WITH CHECK ADD  CONSTRAINT [FK_be_PostTag_be_Posts] FOREIGN KEY([BlogID], [PostID])
REFERENCES [be_Posts] ([BlogID], [PostID])
GO
ALTER TABLE [be_PostTag] CHECK CONSTRAINT [FK_be_PostTag_be_Posts]
GO
/****** Object:  ForeignKey [FK_be_FileStoreFiles_be_FileStoreDirectory]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_FileStoreFiles]  WITH CHECK ADD  CONSTRAINT [FK_be_FileStoreFiles_be_FileStoreDirectory] FOREIGN KEY([ParentDirectoryID])
REFERENCES [be_FileStoreDirectory] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [be_FileStoreFiles] CHECK CONSTRAINT [FK_be_FileStoreFiles_be_FileStoreDirectory]
GO
/****** Object:  ForeignKey [FK_be_FileStoreFileThumbs_be_FileStoreFiles]    Script Date: 04/07/2015 16:09:45 ******/
ALTER TABLE [be_FileStoreFileThumbs]  WITH CHECK ADD  CONSTRAINT [FK_be_FileStoreFileThumbs_be_FileStoreFiles] FOREIGN KEY([FileId])
REFERENCES [be_FileStoreFiles] ([FileID])
ON DELETE CASCADE
GO
ALTER TABLE [be_FileStoreFileThumbs] CHECK CONSTRAINT [FK_be_FileStoreFileThumbs_be_FileStoreFiles]
GO
